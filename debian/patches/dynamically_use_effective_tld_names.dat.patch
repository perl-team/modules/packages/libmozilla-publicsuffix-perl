Description: use effective_tld_names.dat from publicsuffix package
 dynamically at runtime instead of baking it into the module once at build
 time.
Origin: vendor
Forwarded: not-needed
Author: gregor herrmann <gregoa@debian.org>
Last-Update: 2024-08-26

--- a/Build.PL
+++ b/Build.PL
@@ -2,14 +2,7 @@
 
 use strict;
 use warnings FATAL => "all";
-use utf8;
-use open ":encoding(UTF-8)";
-use Carp qw(croak);
-use HTTP::Tiny;
 use Module::Build;
-use IO::File;
-use Tie::File;
-use URI;
 
 my $builder = Module::Build->new(
     dist_name => "Mozilla-PublicSuffix",
@@ -41,41 +34,4 @@
   },
 );
 
-my $dat_file = "effective_tld_names.dat";
-my $get_new_list = $builder->y_n(
-    "Check for a new version of the Public Suffix List?", "N"
-);
-if ($get_new_list) {
-    my $http = HTTP::Tiny->new( timeout => 6 );
-    my $list_uri = URI->new(
-        "https://publicsuffix.org/list/public_suffix_list.dat"
-    );
-    $list_uri->query_form({ raw => 1 });
-    my %options = (
-        headers => {
-            "If-Modified-Since" => "Wed, 14 Aug 2024 01:25:59 GMT"
-        }
-    );
-    my $response = $http->get($list_uri, \%options);
-    if ( $response->{status} == 200 ) {
-        IO::File->new($dat_file, "w")->print($response->{content});
-    }
-    elsif ( $response->{status} != 304 ) {
-        croak "Unable to download public suffix list.";
-    }
-}
-
-# Divide rules from list into sets:
-my $rules = join " ", map {
-    s/\s//g;
-    if    ( s/^!// )        { $_ => "e" }  # exception rule
-    elsif ( s/^\*\.// )     { $_ => "w" }  # wildcard rule
-    elsif ( /^[\p{Word}]/ ) { $_ => "i" }  # identity rule
-    else { () }
-} IO::File->new($dat_file)->getlines;
-
-tie my @pm, "Tie::File", "lib/Mozilla/PublicSuffix.pm";
-for (@pm) { s/(my %rules = qw\()\)/$1$rules)/ and last }
-untie @pm;
-
 $builder->create_build_script;
--- a/lib/Mozilla/PublicSuffix.pm
+++ b/lib/Mozilla/PublicSuffix.pm
@@ -3,8 +3,10 @@
 use strict;
 use warnings FATAL => 'all';
 use utf8;
+use IO::File;
 use Exporter qw(import);
 use URI::_idna;
+use Carp qw(croak);
 
 our @EXPORT_OK = qw(public_suffix);
 
@@ -35,7 +37,17 @@
     return $domain =~ $dn_re ? _find_rule($domain) : ( );
 }
 
-my %rules = qw();
+my $dat_file = '/usr/share/publicsuffix/effective_tld_names.dat';
+croak "Unable to read public suffix list file '$dat_file'." unless -r $dat_file;
+
+# Divide rules from list into sets:
+my %rules = map {
+    s/\s//g;
+    if    ( s/^!// )        { $_ => "e" }  # exception rule
+    elsif ( s/^\*\.// )     { $_ => "w" }  # wildcard rule
+    elsif ( /^[\p{Word}]/ ) { $_ => "i" }  # identity rule
+    else { () }
+} IO::File->new($dat_file, "<:encoding(UTF-8)")->getlines;
 
 # Right-hand side of a domain name:
 sub _rhs {
